FROM ubuntu:22.04 as base
ENV PHP_VERSION 8.1
RUN apt-get update  && DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt-get update && debconf-set-selections <<< "postfix postfix/mailname string your.hostname.com" && \
    debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'" && \
    apt-get install nano python3 python3-pip python3-yaml nginx php8.1 php8.1-fpm php8.1-cli php8.1-common php8.1-curl php8.1-phpdbg php8.1-gd php8.1-odbc php8.1-pgsql php8.1-mbstring php8.1-mysql php8.1-xmlrpc php8.1-opcache php8.1-intl php8.1-bz2 php8.1-xml php8.1-imagick php8.1-pspell php8.1-imap php8.1-gd php8.1-curl php8.1-xmlrpc php8.1-mysql php8.1-cgi php8.1-fpm php8.1-dev php8.1-bcmath php8.1-mbstring php8.1-curl php8.1-dom php8.1-mysql php8.1-zip php8.1-sqlite3 \
    libsasl2-modules postfix rsyslog -y && \
    rm -rf /var/lib/apt/lists/* \
    && sed -i \
        -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" \
        -e "s/memory_limit\s*=\s*.*/memory_limit = 256M/g" \
        -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" \
        -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" \
        -e "s/max_execution_time = 30/max_execution_time = 180/g" \
        -e "s/max_input_time = 60/max_input_time = 180/g" \
        -e "s/variables_order = \"GPCS\"/variables_order = \"EGPCS\"/g" \
        -e "s/;opcache.enable=1/opcache.enable=1/"\
        -e "s/;opcache.memory_consumption=128/opcache.memory_consumption=512/g" \
        -e "s/;opcache.interned_strings_buffer=8/opcache.interned_strings_buffer=64/g" \
        -e "s/;opcache.max_accelerated_files=10000/opcache.max_accelerated_files=30000/g" \
        -e "s/;opcache.revalidate_freq=2/opcache.revalidate_freq=0/g" \
        /etc/php/${PHP_VERSION}/fpm/php.ini \
    && sed -i \
        -e "s/;daemonize\s*=\s*yes/daemonize = no/g" \
        /etc/php/${PHP_VERSION}/fpm/php-fpm.conf \
    && sed -i \
        -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" \
        -e "s/pm.max_children = 5/pm.max_children = 4/g" \
        -e "s/pm.start_servers = 2/pm.start_servers = 3/g" \
        -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 2/g" \
        -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 4/g" \
        -e "s/;pm.max_requests = 500/pm.max_requests = 200/g" \
        -e "s/^;clear_env = no$/clear_env = no/" \
        /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf && \
    sed -i -e "s/worker_processes  1/worker_processes 4/" /etc/nginx/nginx.conf


COPY start.sh /start.sh
RUN chmod 755 /start.sh
USER www-data

EXPOSE 8080
ENTRYPOINT ["/start.sh"]